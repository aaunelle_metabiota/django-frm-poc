# FRM Django REST API Proof of Concept

## Setup

Before starting, I'd recommend setting up a virtual environment using
`virtualenv`. Once that is set up, use `pip install -r
requirements.txt` to install the necessary dependencies for this
project.

### Create and migrate the database
`python manage.py migrate`

### Populate the database with generated sample model data
`./add-test-data.sh`

### Run the server
`python manage.py runserver`
Visit `http://localhost:8000/` and select a one of the REST links. The Web UI
is similar to Swagger. To view the pure REST JSON, append `.json` to the end of
any URL.
