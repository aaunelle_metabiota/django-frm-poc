#!/bin/bash
# Requires https://pypi.python.org/pypi/django-autofixture
python manage.py loadtestdata \
  frmcorporate.CorporateOperatingGroup:100 \
  frmcorporate.CorporateFarmGroup:100 \
  frmcorporate.CorporatePostalZip:100 \
  frmcorporate.CorporateFarm:100 \
  frmcorporate.CorporateHouse:100 \
  frmcorporate.CorporateProcessing:100 \
  frmcorporate.CorporateProcessingLine:100 \
  frmcorporate.CorporateHatchery:100
