from decimal import Decimal
from django.db import models


class CorporateOperatingGroup(models.Model):
    source_id = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    abbreviation = models.CharField(max_length=10)


class CorporateFarmGroup(models.Model):
    operating_group = models.ForeignKey(CorporateOperatingGroup, on_delete=models.CASCADE)
    source_id = models.CharField(max_length=30)
    name = models.CharField(max_length=30)

class CorporatePostalZip(models.Model):
    postal_zip = models.CharField(max_length=45)
    city = models.CharField(max_length=45)
    county = models.CharField(max_length=45)
    state_abbrv = models.CharField(max_length=3)
    state = models.CharField(max_length=45)
    region = models.CharField(max_length=45)
    country_abbrv = models.CharField(max_length=3)
    country = models.CharField(max_length=45)

class CorporateFarm(models.Model):
    operating_group = models.ForeignKey(CorporateOperatingGroup, on_delete=models.CASCADE)
    farm_group = models.ForeignKey(CorporateFarmGroup, on_delete=models.CASCADE)
    stage = models.CharField(max_length=45)
    source_farm_id = models.CharField(max_length=45)
    farm_name = models.CharField(max_length=45)
    house_total = models.IntegerField()
    ownership = models.CharField(max_length=45)
    address_1 = models.CharField(max_length=45)
    address_2 = models.CharField(max_length=45)
    postal_zip = models.ForeignKey(CorporatePostalZip, on_delete=models.CASCADE)
    lattitude = models.DecimalField(max_digits=20,decimal_places=4)
    longitude = models.DecimalField(max_digits=20,decimal_places=4)

class CorporateHouse(models.Model):
    farm = models.ForeignKey(CorporateFarm, on_delete=models.CASCADE)
    source_house_id = models.CharField(max_length=45)
    house = models.IntegerField()
    length = models.IntegerField()
    width = models.IntegerField()
    area = models.IntegerField()
    cfm = models.IntegerField(blank=True)
    head_capacity = models.IntegerField(blank=True)
    bin_capacity = models.IntegerField(blank=True)
    water_line_type = models.CharField(max_length=45)
    feeeder_type = models.CharField(max_length=45)
    insulation_type = models.CharField(max_length=45)
    fan_type = models.CharField(max_length=45)
    light_type = models.CharField(max_length=45)
    fuel_type = models.CharField(max_length=45)
    date_constructed = models.DateTimeField(auto_now_add=True, blank=True)
    date_refurbished = models.DateTimeField(auto_now_add=True, blank=True)
    date_decomisioned = models.DateTimeField(auto_now_add=True, blank=True)

class CorporateProcessing(models.Model):
    operating_group = models.ForeignKey(CorporateOperatingGroup, on_delete=models.CASCADE)
    source_plant_id = models.CharField(max_length=45)
    plant_name = models.CharField(max_length=45)
    plant_name_abbrev = models.CharField(max_length=12)
    ownership = models.CharField(max_length=45)
    address_1 = models.CharField(max_length=45)
    address_2 = models.CharField(max_length=45)
    postal_zip = models.ForeignKey(CorporatePostalZip, on_delete=models.CASCADE)
    lattitude = models.DecimalField(max_digits=20,decimal_places=4)
    longitude = models.DecimalField(max_digits=20,decimal_places=4)
    max_capacity = models.IntegerField()

class CorporateProcessingLine(models.Model):
    source_line_id = models.CharField(max_length=45)
    processing = models.ForeignKey(CorporateProcessing, on_delete=models.CASCADE)
    max_capacity_line = models.IntegerField()
    max_speed = models.IntegerField()
    min_speed = models.IntegerField()
    date_constructed = models.DateTimeField(auto_now_add=True, blank=True)
    date_refurbished = models.DateTimeField(auto_now_add=True, blank=True)
    date_decommisioned = models.DateTimeField(auto_now_add=True, blank=True)

class CorporateHatchery(models.Model):
    operating_group = models.ForeignKey(CorporateOperatingGroup, on_delete=models.CASCADE)
    source_hatchery_id = models.CharField(max_length=45)
    hatchery_name = models.CharField(max_length=45)
    hatchery_abbrev = models.CharField(max_length=12)
    ownership = models.CharField(max_length=45)
    address_1 = models.CharField(max_length=45)
    address_2 = models.CharField(max_length=45)
    postal_zip = models.ForeignKey(CorporatePostalZip, on_delete=models.CASCADE)
    longitude = models.DecimalField(max_digits=20,decimal_places=4)
    lattitude = models.DecimalField(max_digits=20,decimal_places=4)
    cold_storage_capacity = models.IntegerField()
    number_setter = models.IntegerField()
    number_incubators = models.IntegerField()
    number_hatchers = models.IntegerField()
    max_capacity = models.IntegerField()
    date_constructed = models.DateTimeField(auto_now_add=True, blank=True)
    date_refurbished = models.DateTimeField(auto_now_add=True, blank=True)
    date_decomissioned = models.DateTimeField(auto_now_add=True, blank=True)
