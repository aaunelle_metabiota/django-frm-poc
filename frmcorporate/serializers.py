from rest_framework import serializers
from frmcorporate.models import *
from django.contrib.auth.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username')

class CorporateOperatingGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateOperatingGroup
        fields = ('id', 'source_id', 'name', 'abbreviation')

class CorporateFarmGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateFarmGroup
        fields = ('id', 'operating_group', 'source_id', 'name')

class CorporatePostalZipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporatePostalZip
        fields = ('id', 'postal_zip', 'city', 'county', 'state_abbrv', 'state', 'region', 'country_abbrv', 'country')

class CorporateFarmSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateFarm
        fields = ('id', 'operating_group', 'farm_group', 'stage', 'source_farm_id', 'farm_name', 'house_total', 'ownership', 'address_1', 'address_2', 'postal_zip', 'lattitude', 'longitude')

class CorporateHouseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateHouse
        fields = ('id', 'farm', 'source_house_id', 'house', 'length', 'width', 'area', 'cfm', 'head_capacity', 'bin_capacity', 'water_line_type', 'feeeder_type', 'insulation_type', 'fan_type', 'light_type', 'fuel_type', 'date_constructed', 'date_refurbished', 'date_decomisioned')

class CorporateProcessingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateProcessing
        fields = ('id', 'operating_group', 'source_plant_id', 'plant_name', 'plant_name_abbrev', 'ownership', 'address_1', 'address_2', 'postal_zip', 'lattitude', 'longitude', 'max_capacity')

class CorporateProcessingLineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateProcessingLine
        fields = ('id', 'source_line_id', 'processing', 'max_capacity_line', 'max_speed', 'min_speed', 'date_constructed', 'date_refurbished', 'date_decommisioned')

class CorporateHatcherySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CorporateHatchery
        fields = ('id', 'operating_group', 'source_hatchery_id', 'hatchery_name', 'hatchery_abbrev', 'ownership', 'address_1', 'address_2', 'postal_zip', 'longitude', 'lattitude', 'cold_storage_capacity', 'number_setter', 'number_incubators', 'number_hatchers', 'max_capacity', 'date_constructed', 'date_refurbished', 'date_decomissioned')
