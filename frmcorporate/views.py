from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from frmcorporate.models import CorporateFarmGroup, CorporateOperatingGroup
from frmcorporate.permissions import IsOwnerOrReadOnly
from frmcorporate.serializers import *


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class CorporateOperatingGroupViewSet(viewsets.ModelViewSet):
    queryset = CorporateOperatingGroup.objects.all()
    serializer_class = CorporateOperatingGroupSerializer

class CorporateFarmGroupViewSet(viewsets.ModelViewSet):
    queryset = CorporateFarmGroup.objects.all()
    serializer_class = CorporateFarmGroupSerializer

class CorporatePostalZipViewSet(viewsets.ModelViewSet):
    queryset = CorporatePostalZip.objects.all()
    serializer_class = CorporatePostalZipSerializer

class CorporateFarmViewSet(viewsets.ModelViewSet):
    queryset = CorporateFarm.objects.all()
    serializer_class = CorporateFarmSerializer

class CorporateHouseViewSet(viewsets.ModelViewSet):
    queryset = CorporateHouse.objects.all()
    serializer_class = CorporateHouseSerializer

class CorporateProcessingViewSet(viewsets.ModelViewSet):
    queryset = CorporateProcessing.objects.all()
    serializer_class = CorporateProcessingSerializer

class CorporateProcessingLineViewSet(viewsets.ModelViewSet):
    queryset = CorporateProcessingLine.objects.all()
    serializer_class = CorporateProcessingLineSerializer

class CorporateHatcheryViewSet(viewsets.ModelViewSet):
    queryset = CorporateHatchery.objects.all()
    serializer_class = CorporateHatcherySerializer
