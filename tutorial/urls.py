from frmcorporate import views
from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'corporate/operating_group', views.CorporateOperatingGroupViewSet)
router.register(r'corporate/farm_group', views.CorporateFarmGroupViewSet)
router.register(r'corporate/postal_zip', views.CorporatePostalZipViewSet)
router.register(r'corporate/farm', views.CorporateFarmViewSet)
router.register(r'corporate/house', views.CorporateHouseViewSet)
router.register(r'corporate/processing', views.CorporateProcessingViewSet)
router.register(r'corporate/processing_line', views.CorporateProcessingLineViewSet)
router.register(r'corporate/hatchery', views.CorporateHatcheryViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
